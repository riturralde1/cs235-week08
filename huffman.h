/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Br. Helfrich
 * Summary:
 *    This program will implement the huffman functions
 ************************************************************************/

#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <string>
#include <ostream>
#include "pair.h"
#include "node.h"
#include "bnode.h"
using namespace custom;

class hTree
{
private:
   BNode<pair<std::string, float>> *tree;

public:
	hTree(pair<std::string, float> newPair)
	{
		tree = new BNode<pair<std::string, float>>(newPair);
	}

   friend bool operator > (const hTree& lhs, const hTree& rhs)
   {
      return (lhs.tree->data > rhs.tree->data);
   }

   friend hTree operator + (hTree& lhs, hTree& rhs)
   {
      hTree <pair<string, float>> sum;
      sum.tree = lhs.tree->data + rhs.tree->data;

	  if (lhs > rhs)
	  {
		  addLeft(sum, rhs);
		  addRight(sum, lhs);
	  }

	  else
	  {
	     addLeft(sum, lhs);
	     addRight(sum, rhs);
      }

      return sum;
   }

	hTree & operator += (hTree const &rhs);
	friend std::ostream &operator << (std::ostream& out,
       const  hTree &rhs);
};

std::ostream& operator << (std::ostream & out,
   const hTree& rhs)
{
   std::string code = "= "; 
   if (rhs.tree != NULL)
   {
      if (rhs.tree->pLeft)
	  {
		 code += "0";
         out << rhs.tree->pLeft;
		 out << code;
      }

      out << rhs.tree->data << " ";

      if (rhs.tree->pRight)
	  {
		 code += "1";
         out << rhs.tree->pRight;
		 out << code;
      }
   }

   return out;
}

hTree &hTree::operator+= (hTree const& rhs)
{
	tree->data += rhs.tree->data;
	return *this;
}

void huffman(const std::string & fileName);

void makeList(const std::string& fileName, 
   Node<pair<std::string, float>>* list,
   int *size);

void sortList(pair<std::string, float> array[],
   int num);

void nodeToArray(Node<pair<std::string, float>> *node,
   pair<std::string, float> array[], int *size);

void createTree(pair<std::string, float> sortList[],
   int* size);//falta terminar a logica

void newSubTree(
   pair<std::string, float> sortList[],
   int* index, hTree tree);

void newParent(pair<std::string, float> sortList[],
   int* index, BNode <pair<std::string, float>> *oldParent);

#endif // HUFFMAN_h
