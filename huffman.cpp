/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Everton Alves
 *    Rodrigo Iturralde
 * Summary:
 *    This program will implement the huffman() function
 ************************************************************************/

#include <iostream>        // for CIN and COUT
#include <fstream>         // for IFSTREAM
#include <sstream>         // for StringStream
#include <cassert>         // for ASSERT
#include <string>          // for STRING: binary representation of codes
#include "bnode.h"         // for BINARY_NODE class definition
#include "node.h"          // for Node list container
#include "pair.h"          // for PAIR container
#include "huffman.h"       // for HUFFMAN() prototype

using std::cout;
using std::cin;
using std::ifstream;
using std::endl;
using std::string;
using std::bad_alloc;
using std::stringstream;
using namespace custom;

/***************************************************
 * HUFFMAN MAKELIST
 * Read the file and saves the list in pairs
 ***************************************************/
void makeList(const std::string& fileName,
   Node<pair<std::string, float>>* list, int *size)
{
   ifstream fin(fileName);

   // check to see if the file correctly opened 
   if (fin.fail()) 
   {
      cout << "Unable to open: " << fileName << endl;
	  return;
   }

   stringstream file(fileName);
   pair<string, float> take;

   while (!file.eof())
   {
      file >> take;
	  list = insert(list, take, true);
	  size++;
   }

   fin.close();
}

/***********************************************
 * INSERTION SORT
 * Sort the items in the array
 **********************************************/
void sortList(pair<std::string, float> array[],
   int num)
{
   if (num <= 0)
      return;

   // the first two numbers of the array is created like this
   // to avoid bug with the greaterThan function.
   Node<pair<std::string, float>>* pNode =
      new Node<pair<std::string, float>> (array[0]);

   if (pNode->data >= array[1])
      pNode = insert(pNode, array[1]);
   else
      insert(pNode, array[1], true);

   for (int i = 2; i < num; i++)
   {
      //Smallest elements become the new head
      if (pNode->data >= array[i])
         pNode = insert(pNode, array[i]);

      //Otherwise traverse and find where to insert
      else
      {
         Node <pair<std::string, float>>* temporaryNode =
            greaterThan(pNode->pNext, array[i]);

         if (array[i] < temporaryNode->data)
            insert(temporaryNode, array[i]);

         else
            insert(temporaryNode, array[i], true);
      }
   }

   // values add back to the array
   for (int i = 0; i < num; i++)
   {
	   array[i] = pNode->data;
	   pNode = pNode->pNext;
   }	  
}

/********************************************
 * HUFFMAN NODETOARRAY
 * Make a Node list become a array
 *******************************************/
void nodeToArray(Node<pair<string, float>> *node,
   pair<string, float> array[], int *size)
{
   for (const Node<pair<string, float>>* p = node; p; p = p->pNext)
   {
      array[*size] = node->data;
      ++size;
   }
}

/************************************************
 * HUFFMAN CREATETREE
 * Create the Huffman Code Tree
 ************************************************/
void createTree(pair<string, float> sortList[],
   int* size)
{
	hTree parent(sortList[0]);

   // Need to find a way to always update the 
   // parent node to the newest one.
	for (int index = 1; index < *size; index++)
	{
		parent += hTree(sortList[index]);
    }
}

/*******************************************
 * HUFFMAN
 * Driver program to exercise the huffman
 * generation code
 *******************************************/
void huffman(const string & fileName)
{
   Node <pair<string, float>> listFile;
   int sizeArray = 0;

   makeList(fileName, &listFile, &sizeArray);
   pair<string, float> sort[30];

   nodeToArray(&listFile, sort, &sizeArray);
   sortList(sort, sizeArray);

   return;
}
