/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Br. Helfrich
 * Summary:
 *    Class to capture the notion of a single node in a binary tree
 ************************************************************************/

#ifndef BNODE_H
#define BNODE_H

#include <iostream>

template<class T>
class BNode;

template<class T>
void deleteBTree(BNode<T>*& pNode);

template<class T>
class BNode
{

public:
   // members
   T data;
   BNode* pLeft;
   BNode* pRight;
   BNode* pParent;

   // constructors
   BNode() : pLeft(NULL), pRight(NULL), pParent(NULL) { }
   BNode(const T& value) 
      : data(value), pLeft(NULL), pRight(NULL), pParent(NULL) { }
};

/*******************************************
 * SIZE BINARY TREE
 * 
 *******************************************/
template<class T>
int sizeBTree(const BNode<T>* node)
{
   if (node == NULL)
      return 0;
   else
      return sizeBTree(node->pLeft) + sizeBTree(node->pRight) + 1;
}

/*******************************************
 * ADD LEFT
 * 
 *******************************************/
template<class T>
void addLeft(BNode<T>* pNode, const T& t)
{
   if (pNode == NULL)
      return;

   BNode<T>* newNode = new BNode<T>(t);
   pNode->pLeft = newNode;
   newNode->pParent = pNode;
}

/*******************************************
 * ADD RIGHT
 *
 *******************************************/
template<class T>
void addRight(BNode<T>* pNode, const T& t)
{
   if (pNode == NULL)
      return;

   BNode<T>* newNode = new BNode<T>(t);
   pNode->pRight = newNode;
   newNode->pParent = pNode;
}

/*******************************************
 * ADD LEFT
 *
 *******************************************/
template<class T>
void addLeft(BNode<T>* pNode, BNode<T>* pAdd)
{
   if (pNode == NULL)
      return;

   if (pAdd == NULL)
      deleteBTree(pNode->pLeft);
   else
   {
      pNode->pLeft = pAdd;
      pAdd->pParent = pNode;
   }
}

/*******************************************
 * ADD RIGHT
 *
 *******************************************/
template<class T>
void addRight(BNode<T>* pNode, BNode<T>* pAdd)
{
   if (pNode == NULL || pAdd == NULL)
      return;

   if (pAdd == NULL)
      deleteBTree(pNode->pRight);
   else
   {
      pNode->pRight = pAdd;
      pAdd->pParent = pNode;
   }
}

/*******************************************
 * DELETE BINARY TREE
 *
 *******************************************/
template<class T>
void deleteBTree(BNode<T>* &pNode)
{
   if (&pNode == NULL)
      return;

   if (pNode == NULL)
      return;

   deleteBTree(pNode->pLeft);
   deleteBTree(pNode->pRight);
   delete pNode;
   pNode = NULL;
}

/*******************************************
 * COPY BINARY NODE
 *
 *******************************************/
template<class T>
BNode<T>* copyBTree(const BNode<T>* pSrc)
{
   if (pSrc == NULL)
      return NULL;

   BNode<T>* destination = new BNode<T>(pSrc->data);

   destination->pLeft = copyBTree(pSrc->pLeft);
   if (destination->pLeft != NULL)
      destination->pLeft->pParent = destination;

   destination->pRight = copyBTree(pSrc->pRight);
   if (destination->pRight != NULL)
      destination->pRight->pParent = destination;

   return destination;
}

/*******************************************
 * DISPLAY BINARY NODE
 *
 *******************************************/
template<class T>
std::ostream& operator << (std::ostream& out, const BNode<T>* const& bNode)
{
   if (bNode != NULL)
   {
      if (bNode->pLeft)
         out << bNode->pLeft;

      out << bNode->data << " ";

      if (bNode->pRight)
         out << bNode->pRight;
   }

   return out;
}

#endif // !BNODE_H
